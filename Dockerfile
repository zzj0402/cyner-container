FROM nvidia/cuda:12.3.1-base-ubuntu22.04
RUN apt update && apt install -y git vim python3 python3-pip wget software-properties-common
RUN cd /home && git clone https://github.com/aiforsec/CyNER.git
RUN pip install git+https://github.com/aiforsec/CyNER.git 
RUN pip install -r /home/CyNER/requirements.txt
RUN pip install NLTK 
RUN pip install markupsafe==2.0.1